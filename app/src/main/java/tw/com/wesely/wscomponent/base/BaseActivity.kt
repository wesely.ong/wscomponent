package tw.com.wesely.wscomponent.base

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import tw.com.wesely.wscomponent.R
import tw.com.wesely.wscomponent.ui.base.MvpView

open class BaseActivity : AppCompatActivity(), MvpView {
    override fun showMessage(message: String) {
    }

    override fun showMessage(resId: Int) {
    }

    override fun hideKeyboard() {
        val  view = this.getCurrentFocus();
        if (view != null) {
            val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            getSystemService(Context.INPUT_METHOD_SERVICE)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }
}
