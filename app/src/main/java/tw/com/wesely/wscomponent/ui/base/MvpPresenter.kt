package tw.com.wesely.wscomponent.ui.base

import javax.security.auth.callback.Callback

/**
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the MvpView type that wants to be attached with.
 */
interface MvpPresenter<V : MvpView> {

    fun onAttach(mvpView: V)

    fun onDetach()

//    fun handleApiError()

//    fun setUserAsLoggedOut()
}