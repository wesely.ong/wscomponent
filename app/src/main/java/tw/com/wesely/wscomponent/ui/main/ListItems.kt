package tw.com.wesely.wscomponent.ui.main

enum class ListItems {
    ANIMATION,
    CHATS,
    ALBUM,
    MUSIC_LIST,
    BOTTOM_BAR,

}